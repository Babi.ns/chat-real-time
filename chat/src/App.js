import React, { Component } from "react";

import GlobalStyle from "./styles/global";
import Form from "./components/Form/form";

class App extends Component {
  render() {
    return (
      <div>
        <Form />
        <GlobalStyle />
      </div>
    );
  }
}

export default App;