import React, { Component } from "react";

import io from "socket.io-client";

class FormChat extends React.Component{
  constructor(props) {
    super(props);
    this.state = {value: ''}

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('Um nome foi enviado: ' + this.state.value);
    event.preventDefault();
  }
  render() {
    return(
      <form onSubmit={this.handleSubmit}>
        <label>
          <div class="messages"></div>
          <input type="text" value={this.state.value} onChange={this.handleChange} placeholder="Digite sua mensagem" />
        </label>
        <button type="submit" value="Enviar">Enviar</button>
      </form>

    );
  }
}

const socket = io("http://localhost:3000");

export default FormChat;